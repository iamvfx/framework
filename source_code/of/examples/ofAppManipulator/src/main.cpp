#include "ofApp.h"

int main()
{
  ofGLFWWindowSettings settings;
  settings.setGLVersion(2,1);
  auto window = ofCreateWindow(settings);
  auto app = make_shared<ofApp>();
  ofRunApp(window, app);

  return ofRunMainLoop();
}
