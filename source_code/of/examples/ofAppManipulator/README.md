Example for [ofxManipulator][1]
===============================


Dependencies
------------

#### 1. OF 0.9.0 and C++11
#### 2. ofxAssimpModelLoader
#### 3. [ofxFirstPersonCamera](https://github.com/ofnode/ofxFirstPersonCamera)
#### 4. [ofxManipulator](https://github.com/ofnode/ofxManipulator)


Compiling
---------

For [openFrameworks][2]:

[Do all the steps to generate a project folder](https://github.com/ofnode/of/wiki/Compiling-ofApp-with-vanilla-openFrameworks) and replace its `src` folder with the one from this repository.

For [CMake-based openFrameworks][3]:

```bash
git submodule update --init --recursive
mkdir build
cd build
cmake .. -G Ninja -DCMAKE_CXX_COMPILER=clang++
ninja
```

See [ofApp][4] for details.


  [1]: https://github.com/ofnode/ofxManipulator
  [2]: https://github.com/openframeworks/openFrameworks
  [3]: https://github.com/ofnode/of
  [4]: https://github.com/ofnode/ofApp

