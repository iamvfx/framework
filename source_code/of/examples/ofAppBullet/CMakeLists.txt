project(main)
cmake_minimum_required(VERSION 2.8.12)
set(CMAKE_CXX_STANDARD 14)

add_definitions(
  -DTARGET_NO_VIDEO
)

if (WIN32)
  include_directories(
    "${CMAKE_CURRENT_SOURCE_DIR}/../of"
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/glm/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/glew/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/boost/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/fmodex/include"
    ofxBullet
    ofxBullet/events
    ofxBullet/joints
    ofxBullet/render
    ofxBullet/shapes
    bullet-2.82-r2704/src
    bullet-2.82-r2704/src/BulletCollision/CollisionShapes/
    ofxGui/src
    ofxAssimpModelLoader/src
    ../assimp/assimp-4.1.0/include
  )
else()
  include_directories(
    "${CMAKE_CURRENT_SOURCE_DIR}/../of"
    "${CMAKE_CURRENT_SOURCE_DIR}/../redgpu_f/src/glm/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../redgpu_f/src/glew/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../redgpu_f/ubuntu16046/boost_filesystem/include"
    ofxBullet
    ofxBullet/events
    ofxBullet/joints
    ofxBullet/render
    ofxBullet/shapes
    bullet-2.82-r2704/src
    bullet-2.82-r2704/src/BulletCollision/CollisionShapes/
    ofxGui/src
    ofxAssimpModelLoader/src
    ../assimp/assimp-4.1.0/include
  )
endif()

add_executable(
  main
  main.cpp
  ofxBullet/ofxBulletWorldRigid.cpp
  ofxBullet/ofxBulletWorldSoft.cpp
  ofxBullet/joints/ofxBulletJoint.cpp
  ofxBullet/render/GLDebugDrawer.cpp
  ofxBullet/shapes/ofxBulletBaseShape.cpp
  ofxBullet/shapes/ofxBulletBox.cpp
  ofxBullet/shapes/ofxBulletCapsule.cpp
  ofxBullet/shapes/ofxBulletCone.cpp
  ofxBullet/shapes/ofxBulletConvexShape.cpp
  ofxBullet/shapes/ofxBulletCustomShape.cpp
  ofxBullet/shapes/ofxBulletCylinder.cpp
  ofxBullet/shapes/ofxBulletEllipsoid.cpp
  ofxBullet/shapes/ofxBulletPatch.cpp
  ofxBullet/shapes/ofxBulletRigidBody.cpp
  ofxBullet/shapes/ofxBulletRope.cpp
  ofxBullet/shapes/ofxBulletSoftBody.cpp
  ofxBullet/shapes/ofxBulletSoftTriMesh.cpp
  ofxBullet/shapes/ofxBulletSphere.cpp
  ofxBullet/shapes/ofxBulletStaticPlane.cpp
  ofxBullet/shapes/ofxBulletTriMeshShape.cpp
  ofxGui/src/ofxBaseGui.cpp
  ofxGui/src/ofxButton.cpp
  ofxGui/src/ofxColorPicker.cpp
  ofxGui/src/ofxGuiGroup.cpp
  ofxGui/src/ofxInputField.cpp
  ofxGui/src/ofxLabel.cpp
  ofxGui/src/ofxPanel.cpp
  ofxGui/src/ofxSlider.cpp
  ofxGui/src/ofxSliderGroup.cpp
  ofxGui/src/ofxToggle.cpp
  ofxAssimpModelLoader/src/ofxAssimpAnimation.cpp
  ofxAssimpModelLoader/src/ofxAssimpMeshHelper.cpp
  ofxAssimpModelLoader/src/ofxAssimpModelLoader.cpp
  ofxAssimpModelLoader/src/ofxAssimpTexture.cpp
)

if (WIN32)
  if (CMAKE_BUILD_TYPE MATCHES Debug)
    target_link_libraries(
      main
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/boost/lib/vs/x64/libboost_system-vc141-mt-gd-1_64.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/boost/lib/vs/x64/libboost_filesystem-vc141-mt-gd-1_64.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/lib/vs/x64/libpng.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/lib/vs/x64/pixman-1.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/lib/vs/x64/zlib.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/curl/lib/vs/x64/libcurl.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/fmodex/lib/vs/x64/fmodex64_vc.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/FreeImage/lib/vs/x64/FreeImage.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/freetype/lib/vs/x64/libfreetype.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/glew/lib/vs/x64/glew32s.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/glfw/lib/vs/x64/glfw3.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/openssl/lib/vs/x64/libcrypto.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/openssl/lib/vs/x64/libssl.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/pugixml/lib/vs/x64/pugixmld.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/rtAudio/lib/vs/x64/rtAudio.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/tess2/lib/vs/x64/tess2.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/uriparser/lib/vs/x64/uriparser.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/videoInput/lib/vs/x64/videoInput.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/openFrameworksd.lib" #
      opengl32.lib
      winmm.lib
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/BulletDynamicsd.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/BulletCollisiond.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/BulletSoftBodyd.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/LinearMathd.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../assimp/assimp-vc140-mt.lib"
    )
  else()
    target_link_libraries(
      main
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/boost/lib/vs/x64/libboost_system-vc141-mt-1_64.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/boost/lib/vs/x64/libboost_filesystem-vc141-mt-1_64.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/lib/vs/x64/libpng.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/lib/vs/x64/pixman-1.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/cairo/lib/vs/x64/zlib.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/curl/lib/vs/x64/libcurl.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/fmodex/lib/vs/x64/fmodex64_vc.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/FreeImage/lib/vs/x64/FreeImage.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/freetype/lib/vs/x64/libfreetype.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/glew/lib/vs/x64/glew32s.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/glfw/lib/vs/x64/glfw3.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/openssl/lib/vs/x64/libcrypto.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/openssl/lib/vs/x64/libssl.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/pugixml/lib/vs/x64/pugixml.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/rtAudio/lib/vs/x64/rtAudio.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/tess2/lib/vs/x64/tess2.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/uriparser/lib/vs/x64/uriparser.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/vs/videoInput/lib/vs/x64/videoInput.lib"
      "${CMAKE_CURRENT_SOURCE_DIR}/../of/openFrameworks.lib" #
      opengl32.lib
      winmm.lib
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/BulletDynamics.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/BulletCollision.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/BulletSoftBody.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/windows-x64/LinearMath.lib" #
      "${CMAKE_CURRENT_SOURCE_DIR}/../assimp/assimp-vc140-mt.lib"
    )
  endif()
else()
  target_link_libraries(
    main
    -pthread
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/libopenFrameworks.a"
    "${CMAKE_CURRENT_SOURCE_DIR}/../of/libglfw3.a"
    "${CMAKE_CURRENT_SOURCE_DIR}/../redgpu_f/ubuntu16046/boost_filesystem/lib/libboost_filesystem.so.1.71.0"
    -ldl
    -lcurl
    -lz
    -lcairo
    -lfontconfig
    -lfreetype
    -lX11
    -lGL
    -lopenal
    -lmpg123
    -lsndfile
    -lXxf86vm
    -lXrandr
    -lXcursor
    -lXi
    -lXinerama
    "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libBulletDynamics.a"
    "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libBulletCollision.a"
    "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libBulletSoftBody.a"
    "${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libLinearMath.a"
    #"${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libBulletFileLoader.a"
    #"${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libBulletWorldImporter.a"
    #"${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libBulletXmlWorldImporter.a"
    #"${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libConvexDecomposition.a"
    #"${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libGIMPACTUtils.a"
    #"${CMAKE_CURRENT_SOURCE_DIR}/bullet-2.82-r2704-libs/ubuntu1804-x64/libHACD.a"
    "${CMAKE_CURRENT_SOURCE_DIR}/../assimp/libassimp.so.4.1.0"
  )
endif()

if (WIN32)
  if (CMAKE_BUILD_TYPE MATCHES Debug)
    file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/bin/data"                    DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/Debug")
  else()
    file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/bin/data"                    DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/Release")
  endif()
  file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/../of/vcomp120.dll"            DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
  file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/../of/fmodex64.dll"            DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
  file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/../of/libcurl.dll"             DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
  file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/../of/FreeImage.dll"           DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
  file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/../assimp/assimp-vc140-mt.dll" DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
else()
  execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink "${CMAKE_CURRENT_SOURCE_DIR}/bin/data" "${CMAKE_CURRENT_BINARY_DIR}/data")
endif()
