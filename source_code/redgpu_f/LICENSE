
REDGPU, COPYRIGHT (C) 2019 KONSTANTIN ALEXANDROVICH TARASENKOV, AN INDIVIDUAL WITH AN ADDRESS AT
LUNNAYA ULITSA, 11, DOMODEDOVO, MOSCOW REGION, 142000, RUSSIAN FEDERATION

IMPORTANT-READ CAREFULLY: DO NOT INSTALL, COPY OR USE THE ENCLOSED LICENSED MATERIALS, DOCUMENTATION
(AS DEFINED BELOW), OR ANY PORTION THEREOF, (COLLECTIVELY "SOFTWARE") UNTIL YOU HAVE CAREFULLY READ
AND AGREED TO THE FOLLOWING TERMS AND CONDITIONS. THIS IS A LEGAL AGREEMENT ("AGREEMENT") BETWEEN
YOU (EITHER AN INDIVIDUAL OR AN ENTITY) ("LICENSEE") AND KONSTANTIN ALEXANDROVICH TARASENKOV, AN
INDIVIDUAL WITH AN ADDRESS AT LUNNAYA ULITSA, 11, DOMODEDOVO, MOSCOW REGION, 142000, RUSSIAN
FEDERATION ("LICENSOR"). NOTE THAT IF YOU USE THE SOFTWARE ON BEHALF OF AN ENTITY (E.G. YOUR
EMPLOYER), YOU AGREE THAT YOU HAVE AUTHORITY TO BIND SUCH ENTITY TO THE TERMS AND CONDITIONS SET
FORTH IN THIS AGREEMENT. IF YOU DON’T HAVE AUTHORITY TO BIND SUCH ENTITY, YOUR USE AND THE ENTITY’S
USE IS NOT LICENSED.

IF YOU DO NOT AGREE TO THE TERMS OF THIS AGREEMENT, DO NOT INSTALL, COPY OR USE THIS SOFTWARE. BY
INSTALLING, COPYING OR USING THE SOFTWARE YOU AGREE TO ALL THE TERMS AND CONDITIONS OF THIS
AGREEMENT.

THIS SOFTWARE IS LICENSOR CONFIDENTIAL INFORMATION AND MAY NOT BE SHARED WITH ANY THIRD PARTY EXCEPT
AS EXPRESSLY PROVIDED BELOW.

1. DEFINITIONS.

1.1  “Derivative Works” means any work, revision, modification or adaptation made to or derived from
     the Sample Code in whole or in part.

1.2  “Documentation” means documentation, associated, included, or provided in connection with the
     Licensed Materials, or any portion thereof, including but not limited to information provided
     online, electronically, or as install scripts.

1.3  “Free Software License” means an open source or other license that requires, as a condition of
     use, modification or distribution, that any resulting software must be
      (a) disclosed or distributed in source code form;
      (b) licensed for the purpose of making derivative works; or
      (c) redistributable at no charge.

1.4  “Intellectual Property Rights” means all copyrights, trademarks, trade secrets, patents, mask
     works, and all related, similar, or other intellectual property rights recognized in any
     jurisdiction worldwide, including all applications and registrations with respect thereto.

1.5  “Licensed Materials” means the RedGpuSDK (REDGPU Software Development Kit), including:
      (a) Documentation;
      (b) Sample Code;
      (c) tools and utilities;
      (d) Libraries; and
      (e) header files.

1.6  “Licensed Purpose” means the creation of commercial Licensee Software.

1.7  “Licensee Software” means any software developed or modified by Licensee using the Licensed
     Materials, and which may include any Libraries and/or Derivative Works.

1.8  “Libraries” means library files in the Licensed Materials that may be dynamically linked with
     Licensee Software for the Licensed Purpose.

1.9  “Object Code” means the machine readable form of the Licensed Software (as opposed to the human
     readable form of computer programming code) created by or for Licensee by compiling the Source
     Code, or as delivered by Licensor, including the object code version of any Derivative Work.

1.10 “Sample Code” means the header files and Source Code identified within the Licensed Materials
     as sample code.

1.11 “Source Code” means computer programming code in the human readable form and related system
     level documentation, including all associated comments, symbols and any procedural code such as
     job control language.

2. LICENSE.

Subject to the terms and conditions of this Agreement, Licensor hereby grants Licensee a
non-exclusive, royalty-free, revocable, non-transferable, non-assignable copyright license solely
for the Licensed Purpose.

3. RESTRICTIONS.

Except for the limited copyright license expressly granted in Section 2 herein, Licensee has no
other rights in the Licensed Materials, whether express, implied, arising by estoppel or otherwise.
Further restrictions regarding Licensee’s use of the Licensed Materials are set forth below. Except
as expressly authorized herein, Licensee may not:

 (a) distribute or sublicense the Object Code or Source Code of:
      * the Sample Code;
      * Derivative Works; and
      * Libraries
     as incorporated in Licensee Software to customers and end users;
 (b) publish, display, sublicense, assign or otherwise transfer the Licensed Materials (except for
     Libraries required by Licensee Software for the Licensed Purpose);
 (c) decompile, reverse engineer, disassemble or otherwise reduce the Licensed Materials in
     Object Code to a human-perceivable form;
 (d) alter or remove any copyright, trademark or patent notice(s) in the Licensed Materials;
 (e) use the Licensed Materials to:
      * develop inventions directly derived from Confidential Information to seek patent protection;
      * assist in the analysis of Licensee’s patents and patent applications; or
      * modify Licensee’s existing patents or patent applications;
 (f) use the Licensed Materials in way that requires that the Licensed Materials or any portion
     thereof be licensed under a Free Software License; or
 (g) use the Licensed Materials in violation of any applicable law, regulation, generally accepted
     practice, or guidelines in the relevant jurisdictions.

4. THIRD PARTY MATERIALS.

Third party technologies are not licensed as part of the Licensed Materials and are not licensed
under this Agreement. The Licensed Materials may be accompanied by third party software components
(e.g. headers, libraries, sample code) which are licensed to Licensee under the terms and conditions
of their respective licenses located in the directory with the third party software component.

5. OWNERSHIP.

The Licensed Materials including all Intellectual Property Rights therein are and remain the sole
and exclusive property of Licensor, and Licensee shall have no right, title or interest therein
except as expressly set forth in this Agreement.

6. FEEDBACK AND DERIVATIVE WORKS.

Licensee has no obligation to give Licensor any suggestions, comments or other feedback (“Feedback”)
relating to the Licensed Materials or Derivative Works that Licensee Creates. However, Licensor may
use and include any Feedback that it receives from Licensee and Derivative Works that Licensee
creates to improve the Licensed Materials or other Licensor products, software and technologies.
Accordingly, for any Feedback Licensee provides to Licensor or any Derivative Works that Licensee
creates, Licensee grants Licensor and its affiliates and subsidiaries a worldwide, non-exclusive,
irrevocable, royalty-free, fully paid up, perpetual license to, directly or indirectly, use,
reproduce, license, sublicense, distribute, make, have made, sell and otherwise commercialize the
Feedback and Derivative Works in the Licensed Materials or other Licensor products, software and
technologies. Licensee further agrees not to provide any Feedback that (a) Licensee knows is subject
to any Intellectual Property Rights of any third party or (b) is subject to license terms which seek
to require any products incorporating or derived from such Feedback, or other Licensor Intellectual
Property, to be licensed to or otherwise shared with any third party.

7. SUPPORT AND UPDATES.

Licensor is under no obligation to provide any kind of support under this Agreement. Licensor may,
in its sole discretion, provide to Licensee updates to the Licensed Materials, and such updates will
be covered as Licensed Materials under this Agreement.

8. WARRANTY DISCLAIMER, LIMITATION OF LIABILITY AND INDEMNIFICATION.

8.1 Disclaimer of Warranty.

THE LICENSED MATERIALS ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND. LICENSOR DISCLAIMS ALL
WARRANTIES, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, THAT THE LICENSED
MATERIALS WILL RUN UNINTERRUPTED OR ERROR-FREE OR THOSE ARISING FROM CUSTOM OF TRADE OR COURSE OF
USAGE. THE ENTIRE RISK ASSOCIATED WITH THE USE OF THE LICENSED MATERIALS IS ASSUMED BY LICENSEE.
Some jurisdictions do not allow the exclusion of implied warranties, so the above exclusion may not
apply to Licensee.

8.2 Limitation of Liability.

LICENSOR WILL NOT, UNDER ANY CIRCUMSTANCES BE LIABLE TO LICENSEE FOR ANY PUNITIVE, DIRECT,
INCIDENTAL, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING FROM USE OF THE LICENSED MATERIALS OR
THIS AGREEMENT EVEN IF LICENSOR HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

8.3 Indemnification.

Licensee agrees to defend, indemnify and hold harmless Licensor, and any of their directors,
officers, employees, affiliates or agents from and against any and all loss, damage, liability and
other expenses (including reasonable attorneys’ fees), resulting from a) Licensee’s use,
distribution or sublicense of the Licensed Materials, b) violation of the terms and conditions of
this Agreement by Licensee or any sublicensee, or c) for failure by Licensee to obtain and comply
with third party licenses that may be required pursuant to Section 4 herein.

9. CONFIDENTIALITY.

Licensee shall protect the Licensed Materials and any information related thereto (collectively,
“Confidential Information”) by using the same degree of care, but no less than a reasonable degree
of care, to prevent the unauthorized use, dissemination or publication of the Confidential
Information as Licensee uses to protect Licensee’s own confidential information of a like nature.
Licensee shall not disclose any Confidential Information disclosed hereunder to any third party and
shall limit disclosure of Confidential Information to only those of its employees and contractors
with a need to know and who are bound by confidentiality obligations with Licensee at least as
restrictive as those contained in this Agreement. Licensee shall be responsible for Licensee’s
employees and contractors compliance with the terms of this Agreement. Licensee may disclose
Confidential Information in accordance with a judicial or other governmental order, provided that
Licensee either (a) gives Licensor reasonable notice prior to such disclosure to allow Licensor a
reasonable opportunity to seek a protective order or equivalent or (b) obtains written assurance
from the applicable judicial or governmental entity that it will afford the Confidential Information
the highest level of protection afforded under applicable law or regulation.

10. TERMINATION AND SURVIVAL.

Licensor may terminate the Agreement immediately upon the breach by Licensee or any sublicensee of
any of the terms of the Agreement. Licensee may terminate the Agreement upon written notice to
Licensor and destruction of the Licensed Materials Licensee accessed hereunder. The termination of
this Agreement shall:

 (a) immediately result in the termination of all rights granted by Licensee under this Agreement;
     and
 (b) have no effect on any sublicenses previously granted by Licensee to end users under Section 2
     and which are compliant with all terms and conditions of this Agreement, which sublicenses
     shall survive in accordance with their terms.

Upon termination or expiration of this Agreement, Licensee will cease using and destroy or return to
Licensor all copies of the Confidential Information, including but not limited to the Licensed
Materials. Upon termination of this Agreement, all provisions survive except for Section 2.

11. EXPORT RESTRICTIONS.

Licensee shall adhere to all applicable U.S. import/export laws and regulations, as well as the
import/export control laws and regulations of other countries as applicable. Licensee further agrees
they will not export, re-export, or transfer, directly or indirectly, any product, technical data,
software or source code it receives from Licensor, or the direct product of such technical data or
software to any country for which the United States or any other applicable government requires an
export license or other governmental approval without first obtaining such licenses or approvals; or
in violation of any applicable laws or regulations of the United States or the country where the
technical data or software was obtained. Licensee acknowledges the technical data and software
received will not, in the absence of authorization from U.S. or local law and regulations as
applicable, be used by or exported, re-exported or transferred to: (a) any sanctioned or embargoed
country, or to nationals or residents of such countries; (b) any restricted end-user as identified
on any applicable government end-user list; or (c) any party where the end-use involves nuclear,
chemical/biological weapons, rocket systems, or unmanned air vehicles. For the most current Country
Group listings, or for additional information about the EAR or Licensee’s obligations under those
regulations, please refer to the U.S. Bureau of Industry and Security’s website at
https://www.bis.doc.gov/.

12. GOVERNMENT END USERS.

The Licensed Materials are provided with “RESTRICTED RIGHTS.” Use, duplication or disclosure by the
Government is subject to restrictions as set forth in FAR 52.227-14 and DFAR 252.227-7013, et seq.,
or its successor. Use of the Licensed Materials by the Government constitutes acknowledgment of
Licensor’s proprietary rights in it.

13. GENERAL PROVISIONS.

Licensee may not assign this Agreement, any assignment will be null and void. This Agreement may be
executed in multiple counterparts, each of which shall constitute a signed original. Any facsimile
or electronic image of this Agreement or writing referenced herein shall be valid and acceptable for
all purposes as if it were an original. The Parties do not intend that any agency or partnership
relationship be created between them by this Agreement. Each provision of this Agreement shall be
interpreted in such a manner as to be effective and valid under applicable law. However, in the
event that any provision of this Agreement becomes or is declared unenforceable by any court of
competent jurisdiction, such provision shall be deemed deleted and the remainder of this Agreement
shall remain in full force and effect.

14. ENTIRE AGREEMENT.

This Agreement sets forth the entire agreement and understanding between the Parties with respect to
the Licensed Materials and supersedes and merges all prior oral and written agreements, discussions
and understandings between them regarding the subject matter of this Agreement.
