#ifdef _WIN32
#define REDGPU_F_DECLSPEC extern "C" __declspec(dllexport)
#endif
#ifdef __linux__
#define REDGPU_F_DECLSPEC extern "C" __attribute__((visibility("default")))
#endif
#include "redgpu_f.h"

#include "renderer/api/device.h"

REDGPU_F_DECLSPEC RedFHandleRtDevice redFRtDeviceCreateDevice(uint64_t numThreads) {
  return (RedFHandleRtDevice)(void *)embree::Device::rtCreateDevice("default", numThreads);
}